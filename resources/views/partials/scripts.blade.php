<script src="{{ asset('assets/js/vendor/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/vendor/react.js') }}"></script>
<script src="{{ asset('assets/js/vendor/JSXTransformer.js') }}"></script>

<script src="{{ asset('assets/js/classes/Game.js') }}"></script>
<script src="{{ asset('assets/js/classes/Move.js') }}"></script>
<script src="{{ asset('assets/js/classes/Attack.js') }}"></script>
<script src="{{ asset('assets/js/classes/Turn.js') }}"></script>
<script src="{{ asset('assets/js/classes/End.js') }}"></script>
<script src="{{ asset('assets/js/classes/Surrender.js') }}"></script>

<script src="{{ asset('assets/js/components/Unit.js') }}" type="text/jsx"></script>
<script src="{{ asset('assets/js/components/Tile.js') }}" type="text/jsx"></script>
<script src="{{ asset('assets/js/components/Arena.js') }}" type="text/jsx"></script>

@if(config('app.debug'))
    <script type="text/javascript">
        document.write('<script src="//localhost:35729/livereload.js?snipver=1" type="text/javascript"><\/script>')
    </script>
@endif