var Attack = function () {

    var that = this;

    this.show = function (tile, unit) {
        $('.tile').filter(function () {
            var rowNumber = $(this).data('row-id');
            var tileNumber = $(this).data('row-tile-id');
            var rowTileNumber = (Math.abs(rowNumber - tile.rowNumber) + Math.abs(tileNumber - tile.rowTileNumber));
            var rowTileNumber2 = !(rowNumber == tile.rowNumber && tileNumber == tile.rowTileNumber);
            var freeTile = !$(this).hasClass('has-unit');
            var inMoveRange = rowTileNumber <= unit.unit['attack'];

            return inMoveRange && rowTileNumber2 && freeTile;
        }).addClass('attack-option');
    };

    this.attack = function (state, tile) {
        console.log('Damage');
    };

};