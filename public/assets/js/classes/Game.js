var Game = function () {

    var that = this;

    var move = new Move();
    var attack = new Attack();
    var turn = new Turn();
    var end = new End();
    var surrender = new Surrender();

    this.Move = move;
    this.Attack = attack;
    this.Turn = turn;
    this.End = end;
    this.Surrender = surrender;

    this.state = 'move';
    this.selectedUnit = {
        unit: null,
        tile: null
    };

    this.setState = function (state) {
        that.state = state;
    };

    this.handleCurrentState = function (state) {
        var tile = $('[data-tile-id="' + state.tile.tileNumber + '"]');

        if (that.state === 'move') {
            handleMoveState(state, tile);
        }

        if (that.state === 'attack') {
            handleAttackState(state, tile);
        }

        console.log(this.state);
    };

    var handleMoveState = function (state, tile) {
        if (state.unit) {
            that.selectedUnit.unit = state.unit;
            that.selectedUnit.tile = tile;
            removeOptionClass();
            that.Move.show(state.tile, state.unit);
        } else {
            if (tile.hasClass('move-option')) {
                that.Move.move(tile, that.selectedUnit);
                removeOptionClass();
                that.setState('attack');

                that.Attack.show(state.tile, that.selectedUnit);
            }
        }
    };

    var handleAttackState = function (state, tile) {
        if (tile.hasClass('attack-option')) {
            that.selectedUnit.unit = state.unit;
            that.selectedUnit.tile = tile;
            that.Attack.attack(tile, state.unit);
            removeOptionClass();
            that.setState('turn');
        }
    };

    var removeOptionClass = function () {
        $('.tile').removeClass(that.state + '-option');
    };

};