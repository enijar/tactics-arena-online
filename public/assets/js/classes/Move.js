var Move = function () {

    var that = this;

    this.show = function (tile, unit) {
        $('.tile').filter(function () {
            var rowNumber = $(this).data('row-id');
            var tileNumber = $(this).data('row-tile-id');
            var rowTileNumber = (Math.abs(rowNumber - tile.rowNumber) + Math.abs(tileNumber - tile.rowTileNumber));
            var rowTileNumber2 = !(rowNumber == tile.rowNumber && tileNumber == tile.rowTileNumber);
            var freeTile = !$(this).hasClass('has-unit');
            var inMoveRange = rowTileNumber <= unit['movement']['amount'];

            return inMoveRange && rowTileNumber2 && freeTile;
        }).addClass('move-option');
    };

    this.move = function (tile, unit) {
        tile.append(unit.tile.find('.unit').detach());
    };

};