<?php

namespace App\Tao\Units;

class Pyromancer extends Unit
{
    public function __construct()
    {
        $this->name = 'Pyromancer';
        $this->hp = 30;
        $this->power = $this->getPower('damage', 20, false);
        $this->attack = 1;
        $this->armor = 0;
        $this->blocking = $this->getBlocking(33, 16, 0);
        $this->recovery = 3;
        $this->movement = $this->getMovement('normal', 3);

        return $this;
    }
}
