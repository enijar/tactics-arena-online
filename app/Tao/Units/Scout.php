<?php

namespace App\Tao\Units;

class Scout extends Unit
{
    public function __construct()
    {
        $this->name = 'Scout';
        $this->hp = 40;
        $this->power = $this->getPower('damage', 18, true);
        $this->attack = 1;
        $this->armor = 8;
        $this->blocking = $this->getBlocking(60, 30, 0);
        $this->recovery = 2;
        $this->movement = $this->getMovement('normal', 4);

        return $this;
    }
}
