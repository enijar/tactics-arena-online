<?php

namespace App\Tao\Units;

class StoneGolem extends Unit
{
    public function __construct()
    {
        $this->name = 'Stone Golem';
        $this->hp = 60;
        $this->power = $this->getPower('armour', 30, false);
        $this->attack = 1;
        $this->armor = 0;
        $this->blocking = $this->getBlocking(0, 0, 0);
        $this->recovery = 2;
        $this->movement = $this->getMovement('normal', 2);

        return $this;
    }
}
